import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

public class rsa extends  HttpServlet{
	private static final long serialVersionUID = 1L;
	private static final String SIGNING_ALGORITHM= "SHA256withRSA";
	private static final String RSA = "RSA";
	private static Scanner sc;
	
	public void service(HttpServletRequest req,HttpServletResponse res) throws IOException, ServletException{
		
		String input=req.getParameter("key");
		KeyPair keyPair;
		try {
			keyPair = Generate_RSA_KeyPair();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

byte[] signature;
try {
	signature = Create_Digital_Signature(input.getBytes(),keyPair.getPrivate());
} catch (Exception e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
res.getWriter().println("Digital Signature Implemented \n Signature Value:\n "+ DatatypeConverter.printHexBinary(signature));
}
	public static byte[] Create_Digital_Signature(byte[] input,PrivateKey Key)throws Exception
	{
		Signature signature
			= Signature.getInstance(
				SIGNING_ALGORITHM);
		signature.initSign(Key);
		signature.update(input);
		return signature.sign();
	}

	// Generating the asymmetric key pair
	// using SecureRandom class
	// functions and RSA algorithm.
	public static KeyPair Generate_RSA_KeyPair()
		throws Exception
	{
		SecureRandom secureRandom
			= new SecureRandom();
		KeyPairGenerator keyPairGenerator
			= KeyPairGenerator
				.getInstance(RSA);
		keyPairGenerator
			.initialize(
				2048, secureRandom);
		return keyPairGenerator
			.generateKeyPair();
	}

	// Function for Verification of the
	// digital signature by using the public key
	public static boolean Verify_Digital_Signature(byte[] input,byte[] signatureToVerify,PublicKey key)
		throws Exception
	{
		Signature signature
			= Signature.getInstance(
				SIGNING_ALGORITHM);
		signature.initVerify(key);
		signature.update(input);
		return signature
			.verify(signatureToVerify);
	}

}	
	
